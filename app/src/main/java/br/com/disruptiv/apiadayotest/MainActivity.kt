package br.com.disruptiv.apiadayotest

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.adayo.midware.constant.NaviConstantsDef
import com.adayo.midwareproxy.thirdapp.IThirdAppClientListener
import com.adayo.midwareproxy.thirdapp.ThirdAppManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private lateinit var thirdAppManager: ThirdAppManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_main)

        try {
            thirdAppManager = ThirdAppManager.getThirdAppManager()

            etCAN.text = thirdAppManager.canVersion.toString()
            etMCU.text = thirdAppManager.mcuVersion.toString()
            etSO.text = thirdAppManager.osVersion.toString()
            etUUID.text = thirdAppManager.uuidVersion.toString()

            getSpeed()
            getHandBreak()

            thirdAppManager.setThirdAppListener(object : IThirdAppClientListener {
                override fun whileList(p0: String?, p1: Boolean): Boolean {
                    Log.v("API Test", "whileList string: $p0 boolean: $p1")
                    return true
                }

                override fun brakeSignalStateChanged(p0: NaviConstantsDef.ILLUM_NIGHT_STATE?): Boolean {
                    Log.v("API Test", "brakeSignalStateChanged NaviConstantsDef: ${p0?.name}")

                    return true
                }
            })

            thirdAppManager.setThirdAppShoewEnableListener { p0, p1 -> Log.v("API Test", "IThirdAppShowEnableListener callback: string $p0 boolean $p1") }

            getAllAppsInstalled()

        } catch (e: Exception) {
            AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("This firmware isn't ready to use the ThirdApp API`s")
                    .setCancelable(false)
                    .setPositiveButton("Close App") { dialog, _ ->
                        dialog.dismiss()
                        finish()
                    }.show()
        }
    }

    private fun getAllAppsInstalled() {
        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pkgAppsList: List<ResolveInfo> = packageManager.queryIntentActivities(mainIntent, 0)
        Log.v("API Test", "Package List Size: ${pkgAppsList.size}")

        initializeRecyclerView(pkgAppsList.filter {
            !it.activityInfo.packageName.contains("com.adayo") &&
                    !it.activityInfo.packageName.contains("com.android")
        })
    }

    fun initializeRecyclerView(pkgAppsList: List<ResolveInfo>) {
        rvAppsList.layoutManager = LinearLayoutManager(this)
        rvAppsList.adapter = AppsAdapter(pkgAppsList)
    }

    private fun getSpeed() {

        launch {
            val speed = thirdAppManager.speed.toString()
            launch(UI) { etSpeed.text = speed }
            delay(10, TimeUnit.SECONDS)
            getSpeed()
        }
    }

    private fun getHandBreak() {

        launch {
            val handBreak = thirdAppManager.brakeSignal.toString()
            launch(UI) { etHandBreak.text = handBreak }
            delay(10, TimeUnit.SECONDS)
            getHandBreak()
        }
    }


    private fun getLogcat() {

        launch {
            try {
                val process = Runtime.getRuntime().exec("logcat -d")
                val bufferedReader = BufferedReader(
                        InputStreamReader(process.inputStream))

                val log = StringBuilder()
                var line = bufferedReader.readLine()
                while (line != null) {
                    log.append(line)
                    line = bufferedReader.readLine()
                }

                launch(UI) {

                }


            } catch (e: IOException) {
            }

        }

    }

}
