package br.com.disruptiv.apiadayotest

import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.adayo.midwareproxy.thirdapp.ThirdAppManager
import kotlinx.android.synthetic.main.app_item_list.view.*


class AppViewHolder(itemView: View, private val packageManager: PackageManager) : RecyclerView.ViewHolder(itemView) {
    fun buildView(app: ResolveInfo) = with(itemView) {
        ivAppLogo.setBackgroundDrawable(app.loadIcon(packageManager))
        tvAppName.text = app.loadLabel(packageManager)
        tvAppApplicationId.text = app.activityInfo.packageName

        btAddWhiteList.setOnClickListener { ThirdAppManager.getThirdAppManager().setWhileList(app.activityInfo.packageName, true) }
        btRemoveWhiteList.setOnClickListener { ThirdAppManager.getThirdAppManager().setWhileList(app.activityInfo.packageName, false) }
    }
}

class AppsAdapter(private var list: List<ResolveInfo>) : RecyclerView.Adapter<AppViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = AppViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.app_item_list, parent, false), parent.context.packageManager)

    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: AppViewHolder, position: Int) = holder.buildView(list.get(position))
}